# OwnCloud Theme: Light PoC

This is a light theme PoC for OwnCloud.

## Dependencies
 * OwnCloud App [**extendedtheming**](https://bitbucket.org/AubreyHewes/owncloud-extendedtheming)

## Notes
 * Reloads SVGs as inline SVGs; i.e. XML within the DOM; which can then be manipulated.

## Caveats
 * This loads a remote SVG file twice.. once as the original img-src and again for the translation to an inline SVG.
 * The replacement of the loaded SVG with an inline SVG is noticeable (depending on the host speed).


